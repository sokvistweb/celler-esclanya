<section class="cd-section shop">
    <div class="container">
        <div class="text-component">
            <?php if(function_exists('qtranxf_getLanguage')) { ?>
            <?php if (qtranxf_getLanguage()=='ca'): ?>
            <h2>Els Vins</h2>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='es'): ?>
            <h2>Los Vinos</h2>
            <?php endif; ?>
            <?php if (qtranxf_getLanguage()=='en'): ?>
            <h2>Our Wines</h2>
            <?php endif; ?>
            <?php } ?>
        </div>
        
        <ul class="shop-grid products">
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'product', 'order' => 'ASC', 'posts_per_page' => 4 )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <li class="product-card row entry-summary">
                <div class="column column-66">
                    <h2 class="product_title entry-title"><?php the_title(); ?></h2>
                    <div class="details wc-product-details">
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    
                    <?php if( get_field('nota_de_tast') ): ?>
                        <a href="<?php the_field('nota_de_tast'); ?>" class="nota" target="_blank">
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        Nota de tast
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        Nota de cata
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        Tasting note
                        <?php endif; ?>
                        <?php } ?>
                        </a>
                    <?php endif; ?>
                    
                    <?php global $product; ?>
                    
                    <!-- Product price -->
                    <p class="price"><span class="woocommerce-Price-amount amount"><?php echo $product->get_price_html(); ?></span></p>
                    
                    <!-- Quantity & Add to cart button -->
                    <form class="add-to-cart cart" method="post" enctype='multipart/form-data'>
                    <?php
                    /**
                     * @since 2.1.0.
                     */
                    do_action( 'woocommerce_before_add_to_cart_button' );

                    /**
                     * @since 3.0.0.
                     */
                    do_action( 'woocommerce_before_add_to_cart_quantity' );

                    woocommerce_quantity_input( array(
                        'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                        'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                        'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
                    ) );

                    /**
                     * @since 3.0.0.
                     */
                    do_action( 'woocommerce_after_add_to_cart_quantity' );
                    ?>

                    
                    
                    <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" />

                    <button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

                    <?php
                        /**
                         * @since 2.1.0.
                         */
                        do_action( 'woocommerce_after_add_to_cart_button' );
                    ?>
                    </form>
                    <!-- /Quantity & Add to cart button -->
                    
                </div>
                <div class="column column-33 col-image">
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail(); ?>
                    <?php endif; ?>
                </div>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </ul>
        
        <div class="lines-shop"><div class="line"></div><div class="line"></div><div class="line"></div><div class="line"></div></div>
        
    </div>
</section>