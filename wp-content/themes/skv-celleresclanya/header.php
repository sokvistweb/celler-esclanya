<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Petit celler situat a Begur, Girona, que seguint els requisits de la DO Empordà, elabora vi de qualitat de les 3 tipologies, Blanc, Rosat i Negre, alhora que també fa una criança marina." />
    <meta name="keywords" content="celler, vi, Begur, DO Empordà, sotaigua" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="author" href="humans.txt">

    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.ico" rel="shortcut icon">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Volkhov&display=swap" rel="stylesheet">
    <link rel="canonical" href="">
    
    
    <!-- Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Celler Esclanyà - Vins de Begur">
    <meta property="og:description" content="Petit celler situat a Begur, Girona, que seguint els requisits de la DO Empordà, elabora vi de qualitat de les 3 tipologies, Blanc, Rosat i Negre, alhora que també fa una criança marina.">
    <meta property="og:image" content="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/og-image.jpg">
    <meta property="og:url" content="http://www.celleresclanya.com/">
    <meta property="og:site_name" content="Celler Esclanyà">
    
    <!-- Twitter Cards -->
    <meta name="twitter:title" content="Celler Esclanyà - Vins de Begur">
    <meta name="twitter:description" content="Petit celler situat a Begur, Girona, que seguint els requisits de la DO Empordà, elabora vi de qualitat de les 3 tipologies, Blanc, Rosat i Negre, alhora que també fa una criança marina.">
    <meta name="twitter:image" content="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/twitter-image.jpg">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="Celler Esclanyà">
    <meta name="twitter:url" content="http://www.celleresclanya.com/">
    <!-- Validate: https://dev.twitter.com/docs/cards/validation/validator -->
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '2593640230891070'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=2593640230891070&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    
    <?php wp_head(); ?>
    
</head>


<body <?php body_class( ! is_home() && ! is_front_page() ? "is-page" : "" ); ?>>
    
    <div class="eupopup eupopup-top"></div>
    
    <header class="header">
        <div class="wine-label">
            <a href="<?php echo home_url(); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" class="logo-ce" rel="home">
                <svg class="svg-logo-ce"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/symbol-defs.svg#icon-celleresclanya"></use></svg>
            </a>
            <h1><?php bloginfo( 'description' ); ?></h1>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" class="logo" rel="home">
                <svg class="svg-logo"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/symbol-defs.svg#icon-celleresclanya-logo"></use></svg>
            </a>
        </div>
        
        <div class="cd-main-header">
            <div class="cd-main-header__container">
               
                <nav role="navigation" class="page-nav stretchy-nav">
                    <a class="nav-trigger" href="#0"><span aria-hidden="true"></span></a>
                    <ul class="main-menu" id="nav" role="menu">
                        <?php if(function_exists('qtranxf_getLanguage')) { ?>
                        <?php if (qtranxf_getLanguage()=='ca'): ?>
                        <li class="hidden"><a href="#qui-som">Qui som</a></li>
                        <li><a href="#el-celler">El Celler</a></li>
                        <li><a href="#la-vinya">La Vinya</a></li>
                        <li><a href="#els-vins">Els Vins</a></li>
                        <li><a href="#contacte">Contacte</a></li>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='es'): ?>
                        <li class="hidden"><a href="#qui-som">Quienes somos</a></li>
                        <li><a href="#el-celler">La Bodega</a></li>
                        <li><a href="#la-vinya">La Viña</a></li>
                        <li><a href="#els-vins">Los Vinos</a></li>
                        <li><a href="#contacte">Contacto</a></li>
                        <?php endif; ?>
                        <?php if (qtranxf_getLanguage()=='en'): ?>
                        <li class="hidden"><a href="#qui-som">About Us</a></li>
                        <li><a href="#el-celler">The Cellar</a></li>
                        <li><a href="#la-vinya">Vineyards</a></li>
                        <li><a href="#els-vins">Our Wines</a></li>
                        <li><a href="#contacte">Contact</a></li>
                        <?php endif; ?>
                        <?php } ?>
                    </ul>
                </nav>
                
                <!-- .lang-switcher content -->
                <div class="language" id="language-switcher">
                    <?php qtranxf_generateLanguageSelectCode('text') ?>
                </div>
                
                <nav role="navigation">
                    <ul class="shop-menu" role="menu">
                        <li>
                            <a href="/my-account" class="login" title="Accedeix a l'àrea privada">
                                <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/symbol-defs.svg#icon-log-in"></use></svg>
                                <span class="label">Accès usuari</span>
                            </a>
                        </li>
                        <li>
                            <a href="/cart" class="shopping-cart" title="La teva cistella de la compra">
                                <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/symbol-defs.svg#icon-basket"></use></svg>
                                <span class="label">Cistella de la compra</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <!-- WooCommerce cart count -->
                <div class="woo-items-count">
                    <div class="cart-contents-count">
                        <?php echo WC()->cart->get_cart_contents_count(); ?>
                    </div>
                </div>
                <!-- /WooCommerce cart count -->
                
            </div>
        </div>
        
        <div class="lines"><div class="line"></div><div class="line"></div><div class="line"></div><div class="line-h"></div><div class="line-h"></div></div>
        
        <a class="more-btn bounce btn-scroll" href="#qui-som">
            <svg class="svg-logo-ce"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/symbol-defs.svg#icon-arrow-down-b"></use></svg>
        </a>
    </header>
    
    
    <!-- Preloader -->
    <div id="preloader">
        <div id="status" class="spinner">
            <div class="x-reset-container">
                <div class="c-logo">
                    <svg class="c-logo__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 420"><path d="M0 0v420h500V0H0zm304.2 254.6c-12.7 9.8-22.4 22.9-34.7 33-7.2 5.9-13.1 13.2-18.9 20.4-9 11.4-21.1 19.5-32 29.2-14.1 11.5-27.1 24.3-42.6 33.9-15.7 10-31 20.9-48.4 27.8-7.7 3-15.3 6.2-22.5 10.4-6.2 3.2-12.4 7.8-19.6 7.7-15.2.2-30.3-5.1-42.4-14-6.7-4.9-11.7-11.6-17.1-17.9-5.3-6.3-11.2-12.5-14.5-20.3-3.2-7.7-5.6-15.7-7-23.8-2.1-11 .6-22.1 1.2-33.1 1.9-16.6 1.9-33.4 5.2-49.7C14 238.8 24.1 221.4 27 202c3.5-21.4 14.6-40.4 22.4-60.3 6.5-16.8 16.2-32.1 23.5-48.7 9.4-22.1 26.9-39.3 43-56.9 8.5-9.8 21.3-15.6 34.1-16.1 5.3 2.3 10.1 7.5 9.8 13.6-.2 9.8-2.5 19.5-1.5 29.3 1.7 17.8 4.8 35.3 8 52.9 2 10.7 1.6 22.4 7.5 32 6.5 10.7 14.2 20.9 20.9 31.6 5.6 8.8 12.7 16.2 20.4 23.2 6.9 6.2 12.5 14.8 21.9 17.5 12.1 3.7 23.4 10.3 36.1 11.6 10.3 1.1 20.4 3.5 30.6 4.2 11.9 1.5 22.1-6.9 33.9-5.6-11.4 7.8-23.2 14.9-33.4 24.3zm85.7-90.7c-11 7-22.1 14-32 22.5-13.6 12-31.4 18.8-49.3 20.5-9 .9-17.4-3.3-25.5-6.8-6.2-2.8-12.6-4.9-19-7.3-10-3.5-17.7-11.2-25-18.7-19.6-20.6-31.9-46.8-43-72.6-4.4-11.4-4.8-23.6-6.5-35.6-1.4-9.6-1.6-19.8 1.1-29.2 2.3-7.2 8.5-11.9 13.9-16.7 5.8-4.8 11.4-10.3 18.3-13.3 4.3-1.4 8.4 1.5 11.4 4.3 6.8 6.8 9 16.4 12 25.2 8.9 28.5 19.5 57.6 39.9 80.1 11.4 14.7 29.2 21.5 45.6 28.7 13.8 6.3 29.7 7 44.5 4.9 16.6-3 33.2-5.9 50.2-7-11 9-24.7 13.5-36.6 21zm50.7-47.5c-10 3.7-20.9 2.7-30.9 6.2-17.1 5.9-33.7 14.7-52 15.3-10.3.6-19.2-5.3-26.8-11.4-6.8-5.6-14.3-10.3-19.9-17.2-12.5-17.5-16.8-40.8-10.9-61.5 3.6-9.8 8.8-18.8 13.8-27.8 4.4-8.4 13.6-12.1 22.2-14.8 5.2-2 11-.7 15.8 1.5.9 6.3.2 12.7.5 19.2.2 14.6 3.7 29 10.7 41.8 6.3 12.1 16.4 21.5 26.8 30.1 4.2 3.5 8.8 6.8 14.1 8.4 7 1.2 14.1-.4 21.1-1.1 13.5-1.7 27.2-1 40.3 2.5-8.2 3.1-16.6 5.7-24.8 8.8zm14.7-27.3c-3.6.1-6.8-1.9-10.3-2.8-7.3-3.3-15.1 1.6-22.6.4-10.4-1.5-20.6-4.4-29.9-9.3-7.2-3.6-13.1-9.9-14.9-17.9-2.2-9.3-2-19.7 1.2-29 5.2-11.1 15.4-20.4 27.8-22.2 1.5 9.5 1.7 19.8 7.7 27.8 9.4 12.6 21.1 24.5 36.4 29.2 9 2.7 17.5 6.8 25.6 11.7-6.7 4.4-13.2 9.5-21 12.1zM495.1 46c-2.3 6.1-4.6 12.6-10 16.6-2.8 2-6.3.4-9-.7-8.8-4.2-16.9-9.6-25.3-14.7 2.5-7.2 4.1-14.7 4.8-22.2-10.3-1.5-21.9-1.5-29.1-10 7-2.3 14.3-5.4 21.9-3.8 7.5 1.5 14.8 4 22.2 5.9 8.4 2.3 17.3 5.1 23.5 11.6 4.4 4.7 3 11.9 1 17.3z"/></svg>
                    <div class="c-logo__fill"></div>
                </div>
            </div>
        </div>
    </div><!-- /#preloader -->
    
    
    <div class="timeoutPopup timeoutPopup_hidden" id="timeoutPopup">
        <div class="timeoutPopup__wrapper">
            <div class="timeoutPopup__close"></div>
            <div class="timeoutPopup__content">
                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                <p>Enviaments gratuïts a partir de 40€</p>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                <p>Envíos gratuitos a partir de 40€</p>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                <p>Free shipping from €40</p>
                <?php endif; ?>
                <?php } ?>
            </div>
        </div>
    </div>
    
    
    <?php get_template_part( 'content', 'cartdropdown' ); ?>
