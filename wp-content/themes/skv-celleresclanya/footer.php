        
        <div class="eupopup-container eupopup-container-bottomright"> 
            <div class="eupopup-markup">

                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?> 
                <div class="eupopup-body">Aquesta web fa servir cookies. Si continues navegant, considerem que acceptes el seu ús. </div> 
                <div class="eupopup-buttons"> 
                    <a href="#" class="eupopup-button eupopup-button_1">Acceptar</a> 
                    <a href="/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Més info</a> 
                </div> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?> 
                <div class="eupopup-body">Este sitio web usa cookies. Si continúa navegando, consideramos que acepta su uso.</div> 
                <div class="eupopup-buttons"> 
                    <a href="#" class="eupopup-button eupopup-button_1">Aceptar</a> 
                    <a href="/es/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Más info</a> 
                </div> 
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                <div class="eupopup-body">This website is using cookies. By using the website, you agree to this.</div> 
                <div class="eupopup-buttons"> 
                    <a href="#" class="eupopup-button eupopup-button_1">Accept</a> 
                    <a href="/en/politica-de-cookies" target="_blank" class="eupopup-button eupopup-button_2">Learn more</a> 
                </div> 
                <?php endif; ?>
                <?php } ?>

                <div class="clearfix"></div> 
                <a href="#" class="eupopup-closebutton">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a> 
            </div> 
        </div>


        <footer>
            <div class="cd-section footer-section">
                <div class="container">
                    <div class="row">
                        <div class="column column-50">
                            
                            <?php the_field('on_som'); ?>
                            
                            <ul class="social-media">
                                <li class="location"><a href=""><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/symbol-defs.svg#icon-location"></use></svg><span>Google Maps</span></a></li>
                                <li><a href="#contactform" class="fade"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/symbol-defs.svg#icon-at"></use></svg><span>Email</span></a></li>
                                <li><a href="https://www.instagram.com/celler_esclanya/" title="Celler Esclanyà a Instagram" target="_blank"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/symbol-defs.svg#icon-social-instagram"></use></svg><span>Instagram</span></a></li>
                                <li><a href="https://www.facebook.com/celleresclanya/" title="Celler Esclanyà a Facebook" target="_blank"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/symbol-defs.svg#icon-social-facebook"></use></svg><span>Facebook</span></a></li>
                            </ul>
                            
                        </div>

                        <div class="column column-25">
                            <h3>&nbsp;</h3>

                            <ul class="footer-menu">
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                <!--<li><a href="#">Punts de venda</a></li>-->
                                <li><a href="/entrega-i-devolucions/">Entrega i devolucions</a></li>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                <li><a href="/entrega-i-devolucions/">Entrega y devoluciones</a></li>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                <li><a href="/entrega-i-devolucions/">Deliveries and Returns</a></li>
                                <?php endif; ?>
                                <?php } ?>
                            </ul>
                            
                        </div>

                        <div class="column column-25">
                            <h3>Subscriu-te</h3>

                            <p>I t'informarem de les nostres ofertes i novetats</p>

                            <!-- Mailchimp subscribe form -->
                            <form action="https://celleresclanya.us19.list-manage.com/subscribe/post?u=26c4b8d6ad3a4e0124d307aef&amp;id=b730fd22a6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div class="footer-form newsletter-form field field-grouped is-revealing">
                                    <p class="control control-expanded">
                                        <input type="email" value="" name="EMAIL" class="input email" id="mce-EMAIL" placeholder="El teu email" required>
                                    </p>

                                    <p class="control">
                                        <input type="submit" value="Subscriu-te" name="subscribe" id="mc-embedded-subscribe" class="button button primary button-block button-shadow">
                                    </p>
                                </div>
                            </form>
                            <small>Tenim cura de les teves dades, tal com s’explica en la nostra <a href="/politica-de-privacitat/" title="Llegeix la nostra política de privacitat" target="_blank">Política de privacitat</a>.</small>
                            <!-- /Mailchimp subscribe form -->

                        </div>
                    </div>
                </div>

            </div>

            <div class="cd-section subfooter">
                <div class="container">
                    <div class="row">
                        <div class="column col-center">
                            
                            <a class="do-emporda" href="https://www.doemporda.cat/ca/els-cellers/l/165-celler-esclanya.html" title="Web del Consell Regulador de la DO Empordà" target="_blank"><img width="230" height="65" src="<?php echo get_template_directory_uri(); ?>/assets/images/do-emporda.jpg" class="" alt="DO Empordà logo"></a>

                            <ul class="legal-pages">
                                <li>&copy; <?php echo esc_html( date_i18n( __( 'Y', 'blankslate' ) ) ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?></li>
                                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                                <?php if (qtranxf_getLanguage()=='ca'): ?>
                                <li><a href="/avis-legal">Avís legal</a></li>
                                <li><a href="/politica-de-cookies">Política de cookies</a></li>
                                <li><a href="/politica-de-privacitat">Política de privacitat</a></li>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='es'): ?>
                                <li><a href="/avis-legal">Aviso legal</a></li>
                                <li><a href="/politica-de-cookies">Política de cookies</a></li>
                                <li><a href="/politica-de-privacitat">Política de privacidad</a></li>
                                <?php endif; ?>
                                <?php if (qtranxf_getLanguage()=='en'): ?>
                                <li><a href="/avis-legal">Legal notice</a></li>
                                <li><a href="/politica-de-cookies">Cookies policy</a></li>
                                <li><a href="/politica-de-privacitat">Privacy policy</a></li>
                                <?php endif; ?>
                                <?php } ?>
                            </ul>

                        </div>

                    </div>
                </div>
            </div>
            
            <div id="contactform" class="modal">
                <?php if(function_exists('qtranxf_getLanguage')) { ?>
                <?php if (qtranxf_getLanguage()=='ca'): ?>
                <?php echo do_shortcode( '[contact-form-7 id="68" title="Contact Form CAT"]' ); ?>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='es'): ?>
                <?php echo do_shortcode( '[contact-form-7 id="69" title="Contact Form ESP"]' ); ?>
                <?php endif; ?>
                <?php if (qtranxf_getLanguage()=='en'): ?>
                <?php echo do_shortcode( '[contact-form-7 id="39" title="Contact form ENG"]' ); ?>
                <?php endif; ?>
                <?php } ?>
            </div>
            
            <a id="back-to-top" title="Back to top" href="#"></a>
        </footer>
    </main>
    
    
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <!-- Preloader -->
    <script type="text/javascript">
        //<![CDATA[
		$(window).load(function() { // makes sure the whole site is loaded
			$('#status').fadeOut(); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
		})
		//]]>
    </script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/min/plugins.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/min/main.min.js"></script>
    
    <?php wp_footer(); ?>
    
</body>
</html>
