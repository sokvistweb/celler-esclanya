<?php get_header(); ?>


<main>
    <section class="cd-section cd-section--bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
        <div class="container" id="qui-som"></div>
        <div class="overlay"></div>
    </section>
    

    <section class="cd-section">
        <div class="container">
            <div class="row">
                <div class="column column-40 text-volkhov">
                    <?php the_field('qui_som'); ?>
                </div>
                <div class="column column-60">
                    <?php the_field('historia'); ?>
                </div>
            </div>
        </div>
    </section>
    

    <section class="cd-section cd-section--bg-fixed" id="bg-11">
        <div class="pagenav" id="el-celler"></div>
        <div class="container"></div>
        <div class="overlay"></div>
    </section>
    

    <section class="cd-section">
        <div class="container">
            <div class="row">
                <div class="column">
                    <div class="slider image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/el-celler-01.jpg" width="700" height="466" alt="El celler" />
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/el-celler-02.jpg" width="700" height="466" alt="El celler" />
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/el-celler-03.jpg" width="700" height="466" alt="El celler" />
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/el-celler-04.jpg" width="700" height="466" alt="El celler" />
                    </div>
                </div>
                <div class="column">
                    <?php the_field('el_celler'); ?>
                </div>
            </div>
        </div>
    </section>
    

    <section class="cd-section cd-section--bg-fixed" id="bg-21">
        <div class="pagenav" id="la-vinya"></div>
        <div class="container"></div>
        <div class="overlay"></div>
    </section>
    

    <section class="cd-section">
        <div class="container">
            <div class="row">
                <div class="column">
                    <?php the_field('la_vinya'); ?>
                </div>
                <div class="column text-volkhov">
                   
                    <div class="slider image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/la-vinya-01.jpg" width="700" height="466" alt="La vinya" />
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/la-vinya-02.jpg" width="700" height="466" alt="La vinya" />
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/la-vinya-03.jpg" width="700" height="466" alt="La vinya" />
                    </div>
                    
                    <?php the_field('la_vinya_2'); ?>
                    
                </div>
            </div>
        </div>
    </section>


    <section class="cd-section cd-section--bg-fixed" id="bg-31">
        <div class="pagenav" id="els-vins"></div>
        <div class="container"></div>
        <div class="overlay"></div>
    </section>
    
    
    <?php get_template_part( 'content', 'shop' ); ?>
    
    
    <section class="cd-section cd-section--bg-fixed" id="bg-41">
        <div class="pagenav" id="contacte"></div>
        <div class="container"></div>
        <div class="overlay"></div>
    </section>


<?php get_footer(); ?>