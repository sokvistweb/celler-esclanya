<div class="woocommerce widget_shopping_cart shopping-cart-wrap">

    <div class="cart-dropdown">
        <div class="cart-dropdown-inner">
            <?php woocommerce_mini_cart(); ?>
        </div>
    </div><!-- /cart-dropdown -->

</div><!-- /widget_shopping_cart -->