/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

$(document).ready(function() {
    
    
    /* Preloader */
    var	$window = $(window),
        $body = $('body'),
        $header = $('#header'),
        $all = $body.add($header);

    // Disable animations/transitions until the page has loaded.
    $body.addClass('is-loading');

    $window.on('load', function() {
        window.setTimeout(function() {
            $body.removeClass('is-loading');
        }, 0);
    });
    
    
    
    /*! Mobile left nav
    *   http://codyhouse.co/gem/stretchy-navigation/ */
    if( $('.stretchy-nav').length > 0 ) {
		var stretchyNavs = $('.stretchy-nav');
		
		stretchyNavs.each(function(){
			var stretchyNav = $(this),
				stretchyNavTrigger = stretchyNav.find('.nav-trigger');
			
			stretchyNavTrigger.on('click', function(event){
				event.preventDefault();
				stretchyNav.toggleClass('nav-is-visible');
			});
		});

		$(document).on('click', function(event){
			( !$(event.target).is('.nav-trigger') && !$(event.target).is('.nav-trigger span') ) && stretchyNavs.removeClass('nav-is-visible');
		});
	}
    
    
    
    /*  Animated header */
    var changeHeader = 150;
    $(window).scroll(function() {
        var scroll = getCurrentScroll();
        if ( scroll >= changeHeader ) {
            $('header').addClass('scrolled');
            }
            else {
                $('header').removeClass('scrolled');
            }
    });
    
    function getCurrentScroll() {
        return window.pageYOffset;
    }
    
    
    
    
    /* hide-show Mini Cart panel */
    var clickedOnBody = true;
          
    $('.shopping-cart,.widget_shopping_cart').click(function(){
        clickedOnBody  = false;
    });


    $('.widget_shopping_cart').hide();

    $('.shopping-cart,.widget_shopping_cart').hover(function() {
        clearTimeout(timeout);
        $('.widget_shopping_cart').fadeIn(500);
    });

    var timeout;

    function hidepanel() {
        $('.widget_shopping_cart').fadeOut(400); 
    }

    $('.widget_shopping_cart').mouseleave(doTimeout);
    $('.shopping-cart').mouseleave(doTimeout);

    function doTimeout(){
        clearTimeout(timeout);
        timeout = setTimeout(hidepanel, 300);
    }

    $("html").click(function(){
        if (clickedOnBody){
            $('.widget_shopping_cart').hide();
        }
        clickedOnBody=true;
    });
    
    
    
    /* Language Switcher */
    var hoverTimeout, keepOpen = false, stayOpen = $('.language');
    
    $(document).on('mouseenter','.language',function(){
        clearTimeout(hoverTimeout);
        stayOpen.addClass('show');
    }).on('mouseleave','.language',function(){
        clearTimeout(hoverTimeout);
        hoverTimeout = setTimeout(function(){
            if(!keepOpen){
                stayOpen.removeClass('show');   
            }
        },300);
    });
    // Reorder active link
    $('.active').prependTo('.language-chooser');
    
    
    
    /* One Page Nav */
    /* http://github.com/davist11/jQuery-One-Page-Nav */
    $('.main-menu').onePageNav({
		currentClass: 'current',
		changeHash: true,
		scrollSpeed: 1300,
		scrollThreshold: 0.5,
		filter: '',
		easing: 'swing'
	});
    
    
    /* Smooth scrolling */
    $('.btn-scroll').click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, {
            duration: 700,
            easing: 'swing'
        });
        return false;
    });
    
    $('.btn-scroll-long').click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, {
            duration: 1500,
            easing: 'swing'
        });
        return false;
    });
    
    
    
    // sss Slider
    $('.slider').sss({
        slideShow : true, // Set to false to prevent SSS from automatically animating.
        startOn : 0, // Slide to display first. Uses array notation (0 = first slide).
        transition : 1000, // Length (in milliseconds) of the fade transition.
        speed : 4000, // Slideshow speed in milliseconds.
        showNav : true // Set to false to hide navigation arrows.
    });
    
    
    /*
    A simple jQuery modal (http://github.com/kylefox/jquery-modal)
    Version 0.9.2
    */
    $('.fade').click(function(event) {
        $(this).modal({
            fadeDuration: 50
        });
    });    
        
    
    // Back to top
    var scroll_top_duration = 1500,
        $back_to_top = $('#back-to-top');

    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
            }, scroll_top_duration
        );
    });
    
    
    
    // Input number custom style
    // https://codepen.io/komarovdesign/pen/PPRbgb
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
    
    
    /* WooCommerce */
    
    
});


/**
 *
 * JQUERY EU COOKIE LAW POPUPS
 * version 1.1.1
 *
 * Code on Github:
 * https://github.com/wimagguc/jquery-eu-cookie-law-popup
 *
 * To see a live demo, go to:
 * http://www.wimagguc.com/2018/05/gdpr-compliance-with-the-jquery-eu-cookie-law-plugin/
 *
 * by Richard Dancsi
 * http://www.wimagguc.com/
 *
 */

(function($) {

// for ie9 doesn't support debug console >>>
if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };
// ^^^

$.fn.euCookieLawPopup = (function() {

	var _self = this;

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PARAMETERS (MODIFY THIS PART) //////////////////////////////////////////////////////////////
	_self.params = {
		cookiePolicyUrl : 'http://www.wimagguc.com/?cookie-policy',
		popupPosition : 'top',
		colorStyle : 'default',
		compactStyle : false,
		popupTitle : 'This website is using cookies',
		popupText : 'We use cookies to ensure that we give you the best experience on our website. If you continue without changing your settings, we\'ll assume that you are happy to receive all cookies on this website.',
		buttonContinueTitle : 'Continue',
		buttonLearnmoreTitle : 'Learn&nbsp;more',
		buttonLearnmoreOpenInNewWindow : true,
		agreementExpiresInDays : 30,
		autoAcceptCookiePolicy : false,
		htmlMarkup : 'null'
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// VARIABLES USED BY THE FUNCTION (DON'T MODIFY THIS PART) ////////////////////////////////////
	_self.vars = {
		INITIALISED : false,
		HTML_MARKUP : null,
		COOKIE_NAME : 'EU_COOKIE_LAW_CONSENT'
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PRIVATE FUNCTIONS FOR MANIPULATING DATA ////////////////////////////////////////////////////

	// Overwrite default parameters if any of those is present
	var parseParameters = function(object, markup, settings) {

		if (object) {
			var className = $(object).attr('class') ? $(object).attr('class') : '';
			if (className.indexOf('eupopup-top') > -1) {
				_self.params.popupPosition = 'top';
			}
			else if (className.indexOf('eupopup-fixedtop') > -1) {
				_self.params.popupPosition = 'fixedtop';
			}
			else if (className.indexOf('eupopup-bottomright') > -1) {
				_self.params.popupPosition = 'bottomright';
			}
			else if (className.indexOf('eupopup-bottomleft') > -1) {
				_self.params.popupPosition = 'bottomleft';
			}
			else if (className.indexOf('eupopup-bottom') > -1) {
				_self.params.popupPosition = 'bottom';
			}
			else if (className.indexOf('eupopup-block') > -1) {
				_self.params.popupPosition = 'block';
			}
			if (className.indexOf('eupopup-color-default') > -1) {
				_self.params.colorStyle = 'default';
			}
			else if (className.indexOf('eupopup-color-inverse') > -1) {
				_self.params.colorStyle = 'inverse';
			}
			if (className.indexOf('eupopup-style-compact') > -1) {
				_self.params.compactStyle = true;
			}
		}

		if (markup) {
			_self.params.htmlMarkup = markup;
		}

		if (settings) {
			if (typeof settings.cookiePolicyUrl !== 'undefined') {
				_self.params.cookiePolicyUrl = settings.cookiePolicyUrl;
			}
			if (typeof settings.popupPosition !== 'undefined') {
				_self.params.popupPosition = settings.popupPosition;
			}
			if (typeof settings.colorStyle !== 'undefined') {
				_self.params.colorStyle = settings.colorStyle;
			}
			if (typeof settings.popupTitle !== 'undefined') {
				_self.params.popupTitle = settings.popupTitle;
			}
			if (typeof settings.popupText !== 'undefined') {
				_self.params.popupText = settings.popupText;
			}
			if (typeof settings.buttonContinueTitle !== 'undefined') {
				_self.params.buttonContinueTitle = settings.buttonContinueTitle;
			}
			if (typeof settings.buttonLearnmoreTitle !== 'undefined') {
				_self.params.buttonLearnmoreTitle = settings.buttonLearnmoreTitle;
			}
			if (typeof settings.buttonLearnmoreOpenInNewWindow !== 'undefined') {
				_self.params.buttonLearnmoreOpenInNewWindow = settings.buttonLearnmoreOpenInNewWindow;
			}
			if (typeof settings.agreementExpiresInDays !== 'undefined') {
				_self.params.agreementExpiresInDays = settings.agreementExpiresInDays;
			}
			if (typeof settings.autoAcceptCookiePolicy !== 'undefined') {
				_self.params.autoAcceptCookiePolicy = settings.autoAcceptCookiePolicy;
			}
			if (typeof settings.htmlMarkup !== 'undefined') {
				_self.params.htmlMarkup = settings.htmlMarkup;
			}
		}

	};

	var createHtmlMarkup = function() {

		if (_self.params.htmlMarkup) {
			return _self.params.htmlMarkup;
		}

		var html =
			'<div class="eupopup-container' +
			    ' eupopup-container-' + _self.params.popupPosition +
			    (_self.params.compactStyle ? ' eupopup-style-compact' : '') +
				' eupopup-color-' + _self.params.colorStyle + '">' +
				'<div class="eupopup-head">' + _self.params.popupTitle + '</div>' +
				'<div class="eupopup-body">' + _self.params.popupText + '</div>' +
				'<div class="eupopup-buttons">' +
				  '<a href="#" class="eupopup-button eupopup-button_1">' + _self.params.buttonContinueTitle + '</a>' +
				  '<a href="' + _self.params.cookiePolicyUrl + '"' +
				 	(_self.params.buttonLearnmoreOpenInNewWindow ? ' target=_blank ' : '') +
					' class="eupopup-button eupopup-button_2">' + _self.params.buttonLearnmoreTitle + '</a>' +
				  '<div class="clearfix"></div>' +
				'</div>' +
				'<a href="#" class="eupopup-closebutton">x</a>' +
			'</div>';

		return html;
	};

	// Storing the consent in a cookie
	var setUserAcceptsCookies = function(consent) {
		var d = new Date();
		var expiresInDays = _self.params.agreementExpiresInDays * 24 * 60 * 60 * 1000;
		d.setTime( d.getTime() + expiresInDays );
		var expires = "expires=" + d.toGMTString();
		document.cookie = _self.vars.COOKIE_NAME + '=' + consent + "; " + expires + ";path=/";

		$(document).trigger("user_cookie_consent_changed", {'consent' : consent});
	};

	// Let's see if we have a consent cookie already
	var userAlreadyAcceptedCookies = function() {
		var userAcceptedCookies = false;
		var cookies = document.cookie.split(";");
		for (var i = 0; i < cookies.length; i++) {
			var c = cookies[i].trim();
			if (c.indexOf(_self.vars.COOKIE_NAME) == 0) {
				userAcceptedCookies = c.substring(_self.vars.COOKIE_NAME.length + 1, c.length);
			}
		}

		return userAcceptedCookies;
	};

	var hideContainer = function() {
		// $('.eupopup-container').slideUp(200);
		$('.eupopup-container').animate({
			opacity: 0,
			height: 0
		}, 200, function() {
			$('.eupopup-container').hide(0);
		});
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PUBLIC FUNCTIONS  //////////////////////////////////////////////////////////////////////////
	var publicfunc = {

		// INITIALIZE EU COOKIE LAW POPUP /////////////////////////////////////////////////////////
		init : function(settings) {

			parseParameters(
				$(".eupopup").first(),
				$(".eupopup-markup").html(),
				settings);

			// No need to display this if user already accepted the policy
			if (userAlreadyAcceptedCookies()) {
        $(document).trigger("user_cookie_already_accepted", {'consent': true});
				return;
			}

			// We should initialise only once
			if (_self.vars.INITIALISED) {
				return;
			}
			_self.vars.INITIALISED = true;

			// Markup and event listeners >>>
			_self.vars.HTML_MARKUP = createHtmlMarkup();

			if ($('.eupopup-block').length > 0) {
				$('.eupopup-block').append(_self.vars.HTML_MARKUP);
			} else {
				$('BODY').append(_self.vars.HTML_MARKUP);
			}

			$('.eupopup-button_1').click(function() {
				setUserAcceptsCookies(true);
				hideContainer();
				return false;
			});
			$('.eupopup-closebutton').click(function() {
				setUserAcceptsCookies(true);
				hideContainer();
				return false;
			});
			// ^^^ Markup and event listeners

			// Ready to start!
			$('.eupopup-container').show();

			// In case it's alright to just display the message once
			if (_self.params.autoAcceptCookiePolicy) {
				setUserAcceptsCookies(true);
			}

		}

	};

	return publicfunc;
});

$(document).ready( function() {
	if ($(".eupopup").length > 0) {
		$(document).euCookieLawPopup().init({
			'info' : 'YOU_CAN_ADD_MORE_SETTINGS_HERE',
			'popupTitle' : '',
            'popupText' : '',
            'buttonContinueTitle' : '',
            'buttonLearnmoreTitle' : '',
            'popupPosition' : 'hide',
            'htmlMarkup' : ''
		});
	}
});

$(document).bind("user_cookie_consent_changed", function(event, object) {
	console.log("User cookie consent changed: " + $(object).attr('consent') );
});

}(jQuery));



// jQuery timeout modal
// https://www.jqueryscript.net/lightbox/Open-Modal-After-Timeout-jQuery-timeoutModal.html
(function ($) {

    var TimeoutPopup = function (popuptItem, timeoutNumber) {
        /**
         * Функция появления попапа после таймаута  
         * The function of popup after timeout
         */

        var self = this;
        var timeoutMinutes = timeoutNumber * 500;
        var getCookie = $.cookie('timeoutPopup');

        self.setPopupCookie = function () {
            /**
             * Задаём куку при помощи плагина jQuery Cookie 
             * Set cookie with jQuery Cookie plugin
             */
            $.cookie('timeoutPopup', {
                expires: 1,
                path: '/'
            });
        }

        self.openPopup = function () {
            /**
             * Перед открытием попапа проверим наличие куки
             * Before opening the popup, check the availability of cookies
             * и не будет ничего делать если её нету
             * and will not do anything if she is not
             */
            if (getCookie) {
                return false;
            } else {
                self.setPopupCookie();
                $(popuptItem).removeClass('timeoutPopup_hidden');
            }
        }

        self.closePopupHandler = function () {
            /**
             * Обработчик закрытия попапа
             * Popup close handler
             */
            $(popuptItem).on('click', function (event) {
                if ($(event.target).is(popuptItem) || $(event.target).is('.timeoutPopup__close')) {
                    $(popuptItem).addClass('timeoutPopup_hidden');
                } else {
                    return false;
                }

            });
        }

        self.timeoutEnd = function () {
            /**
             * Устанавливаем значение секунд таймаута,
             * по истечение которой будет открываться попап
             * Set the value of the timeout seconds, after which a popup will open
             */
            setTimeout(function () {
                self.openPopup();
            }, timeoutMinutes);
        }

        self.init = function () {
            /**
             * Для начала проверим наличие попапа в DOM
             * First, check for a popup in the DOM
             */
            if ($(popuptItem).length != 0) {
                self.timeoutEnd();
                self.closePopupHandler();
            } else {
                return false;
            }
        }

    }

    $(document).ready(function () {

        var openTimeoutPopup = new TimeoutPopup('#timeoutPopup', 2);
        openTimeoutPopup.init();

    });

})(jQuery);

