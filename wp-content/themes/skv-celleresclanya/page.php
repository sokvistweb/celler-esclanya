<?php get_header(); ?>

<main>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <section id="post-<?php the_ID(); ?>" <?php post_class( 'page-section cd-section' ); ?>>
        <div class="container">

            <h2 class="page-title"><?php the_title(); ?></h2>

            <div class="page-content">
                
                <?php the_content(); ?>
                
            </div>
        </div>
    </section>
    <?php endwhile; endif; ?>
</main>

<?php get_footer(); ?>
