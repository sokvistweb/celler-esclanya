/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

$(document).ready(function() {
    
    
    /* Preloader */
    var	$window = $(window),
        $body = $('body'),
        $header = $('#header'),
        $all = $body.add($header);

    // Disable animations/transitions until the page has loaded.
    $body.addClass('is-loading');

    $window.on('load', function() {
        window.setTimeout(function() {
            $body.removeClass('is-loading');
        }, 0);
    });
    
    
    
    /*! Mobile left nav
    *   http://codyhouse.co/gem/stretchy-navigation/ */
    if( $('.stretchy-nav').length > 0 ) {
		var stretchyNavs = $('.stretchy-nav');
		
		stretchyNavs.each(function(){
			var stretchyNav = $(this),
				stretchyNavTrigger = stretchyNav.find('.nav-trigger');
			
			stretchyNavTrigger.on('click', function(event){
				event.preventDefault();
				stretchyNav.toggleClass('nav-is-visible');
			});
		});

		$(document).on('click', function(event){
			( !$(event.target).is('.nav-trigger') && !$(event.target).is('.nav-trigger span') ) && stretchyNavs.removeClass('nav-is-visible');
		});
	}
    
    
    
    /*  Animated header */
    var changeHeader = 150;
    $(window).scroll(function() {
        var scroll = getCurrentScroll();
        if ( scroll >= changeHeader ) {
            $('header').addClass('scrolled');
            }
            else {
                $('header').removeClass('scrolled');
            }
    });
    
    function getCurrentScroll() {
        return window.pageYOffset;
    }
    
    
    
    
    /* hide-show Mini Cart panel */
    var clickedOnBody = true;
          
    $('.shopping-cart,.widget_shopping_cart').click(function(){
        clickedOnBody  = false;
    });


    $('.widget_shopping_cart').hide();

    $('.shopping-cart,.widget_shopping_cart').hover(function() {
        clearTimeout(timeout);
        $('.widget_shopping_cart').fadeIn(500);
    });

    var timeout;

    function hidepanel() {
        $('.widget_shopping_cart').fadeOut(400); 
    }

    $('.widget_shopping_cart').mouseleave(doTimeout);
    $('.shopping-cart').mouseleave(doTimeout);

    function doTimeout(){
        clearTimeout(timeout);
        timeout = setTimeout(hidepanel, 300);
    }

    $("html").click(function(){
        if (clickedOnBody){
            $('.widget_shopping_cart').hide();
        }
        clickedOnBody=true;
    });
    
    
    
    /* Language Switcher */
    var hoverTimeout, keepOpen = false, stayOpen = $('.language');
    
    $(document).on('mouseenter','.language',function(){
        clearTimeout(hoverTimeout);
        stayOpen.addClass('show');
    }).on('mouseleave','.language',function(){
        clearTimeout(hoverTimeout);
        hoverTimeout = setTimeout(function(){
            if(!keepOpen){
                stayOpen.removeClass('show');   
            }
        },300);
    });
    
    
    
    // One Page Nav
    $('.main-menu').onePageNav({
		currentClass: 'current',
		changeHash: true,
		scrollSpeed: 1300,
		scrollThreshold: 1.5,
		filter: '',
		easing: 'swing'
	});
    
    
    /* Smooth scrolling */
    $('.btn-scroll').click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, {
            duration: 700,
            easing: 'swing'
        });
        return false;
    });
    
    $('.btn-scroll-long').click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, {
            duration: 1500,
            easing: 'swing'
        });
        return false;
    });
    
    
    
    // sss Slider
    $('.slider').sss({
        slideShow : true, // Set to false to prevent SSS from automatically animating.
        startOn : 0, // Slide to display first. Uses array notation (0 = first slide).
        transition : 1000, // Length (in milliseconds) of the fade transition.
        speed : 4000, // Slideshow speed in milliseconds.
        showNav : true // Set to false to hide navigation arrows.
    });
    
        
    
    // Back to top
    var scroll_top_duration = 1500,
        $back_to_top = $('#back-to-top');

    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
            }, scroll_top_duration
        );
    });
    
    
    
    // Input number custom style
    // https://codepen.io/komarovdesign/pen/PPRbgb
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
    
    
});

