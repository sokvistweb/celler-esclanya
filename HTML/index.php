<?php include("header.php"); ?>


<main>
    <section class="cd-section cd-section--bg-fixed" id="bg-0<?php echo(rand(1,4)); ?>">
        <div class="container" id="qui-som"></div>
        <div class="overlay"></div>
    </section>
    

    <section class="cd-section">
        <div class="container">
            <div class="row">
                <div class="column column-40 text-volkhov">
                    <h2>Qui som</h2>
                    <p>El celler neix a partir d’una iniciativa familiar, compromesa amb el territori de l’Empordà i amb la il·lusió d’elaborar un vi de qualitat.</p>

                    <p>Aquesta iniciativa es va consolidar l’any 2016 després d’un llarg recorregut en l’elaboració de vi a nivell particular.</p>
                    
                    <a href="#contacte" class="button btn-scroll-long">Contacta amb nosaltres</a>
                </div>
                <div class="column column-60">
                    <h2>Història</h2>
                    <p>Des de 1830 al celler del Mas Nou (nom del mas que hi ha el celler) ha estat regentat per la família Carbó. Duran tots aquests anys han anat passat de generacions en generacions la tradició d’elaborar el seu propi vi a través del cultiu acurat dels ceps i una metòdica fermentació.</p>

                    <p>Aquesta tradició amb els anys s’ha anat professionalitzant i degut a una voluntat d’expandir aquest mon, la quarta generació de la família decideix oficialitzar-ho i començar a comercialitzar el seu propi vi. Un vi elaborat amb la filosofia tradicional però amb els tocs moderns que poden aportar les noves generacions.</p>
                </div>
            </div>
        </div>
    </section>
    

    <section class="cd-section cd-section--bg-fixed" id="bg-11">
        <div class="container" id="el-celler"></div>
        <div class="overlay"></div>
    </section>
    

    <section class="cd-section">
        <div class="container">
            <div class="row">
                <div class="column">
                    <div class="slider image">
                        <img src="assets/images/el-celler-01.jpg" alt="El celler" />
                        <img src="assets/images/el-celler-02.jpg" alt="El celler" />
                        <img src="assets/images/el-celler-03.jpg" alt="El celler" />
                        <img src="assets/images/el-celler-04.jpg" alt="El celler" />
                    </div>
                </div>
                <div class="column">
                    <h2>El Celler</h2>

                    <p>Aquí és on elaborem els nostres vins, disposem d’unes noves infraestructures, equipades per tal de realitzar uns vins amb una minuciosa fermentació controlant tots els factors externs possibles a través de la climatització controlada.</p>
                    
                    <a href="#els-vins" class="button btn-scroll">Descobreix els nostres vins</a>
                </div>
            </div>
        </div>
    </section>
    

    <section class="cd-section cd-section--bg-fixed" id="bg-21">
        <div class="container" id="la-vinya"></div>
        <div class="overlay"></div>
    </section>
    

    <section class="cd-section">
        <div class="container">
            <div class="row">
                <div class="column">
                    <h2>La Vinya</h2>
                    
                    <p>Totes les nostres vinyes estan al terme municipal de Begur, un municipi costaner endinsat al Mar Mediterrani i amb un toc de Tramuntana, el vent més característic de l’Empordà.</p>

                    <p>Disposem de les varietats negres de Merlot, Garnatxa Negra i Cabernet Sauvingnon. I en blanc disposem de Malvasia, Petit Manseng, Marsanne i Moscat. Una part d’ells utilitzem l’agricultura moderna en forma d’emparrat a fi efecte d’optimitzar el fruït que ens ofereixen els nostres ceps, d’altres disposem de vinyes velles per tal de donar el toc d’experiència que ens ofereixen. </p>
                    
                    <a href="#els-vins" class="button btn-scroll">Descobreix els nostres vins</a>
                </div>
                <div class="column text-volkhov">
                   
                    <div class="slider image">
                        <img src="assets/images/la-vinya-01.jpg" alt="La vinya" />
                        <img src="assets/images/la-vinya-02.jpg" alt="La vinya" />
                        <img src="assets/images/la-vinya-03.jpg" alt="La vinya" />
                    </div>

                    <p>Tots els nostres vins estan recollits a mà, a fi efecte d’aconseguir una millor qualitat, i només elaborem vi amb les vinyes que treballem nosaltres per tal de poder conèixer exactament l’estat del raïm.</p>
                    
                </div>
            </div>
        </div>
    </section>


    <section class="cd-section cd-section--bg-fixed" id="bg-31">
        <div class="container" id="els-vins"></div>
        <div class="overlay"></div>
    </section>

        
    <?php include("shop.php"); ?>


    <section class="cd-section cd-section--bg-fixed" id="bg-41">
        <div class="container" id="contacte"></div>
        <div class="overlay"></div>
    </section>


<?php include("footer.php"); ?>