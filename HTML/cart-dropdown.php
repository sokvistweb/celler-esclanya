<div class="woocommerce widget_shopping_cart">

    <!--<div class="cart-dropdown">
        <div class="cart-dropdown-inner">
            <div class="dropdown-cart-wrap isempty">
                <p>Tu bolsa de la compra está vacía.</p>
            </div>
        </div>
    </div>-->

    <div class="cart-dropdown">
        <div class="cart-dropdown-inner">

            <ul class="woocommerce-mini-cart cart_list product_list_widget ">
                <li class="dropdown-cart-wrap woocommerce-mini-cart-item mini_cart_item">
                    <a href="#" class="remove remove_from_cart_button" aria-label="Borrar este artículo">×</a>
                        <a class="wc-thumb-title" href="#">
                            <img src="assets/images/blanc-2018-celler-esclanya.jpg" alt="Celler Esclanyà Blanc 2018">Blanc 2018
                        </a>
                    <span class="quantity">1 × <span class="woocommerce-Price-amount amount">90,00<span class="woocommerce-Price-currencySymbol">€</span></span></span>
                </li>

                <li class="dropdown-cart-wrap woocommerce-mini-cart-item mini_cart_item">
                    <a href="#" class="remove remove_from_cart_button">×</a>							<a class="wc-thumb-title" href="#">
                            <img src="assets/images/negre-2018-celler-esclanya.jpg" alt="Celler Esclanyà Negre 2018">Negre 2018
                        </a>
                    <span class="quantity">1 × <span class="woocommerce-Price-amount amount">90,00<span class="woocommerce-Price-currencySymbol">€</span></span></span>	
                </li>

                <li class="dropdown-cart-wrap woocommerce-mini-cart-item mini_cart_item">
                    <a href="#" class="remove remove_from_cart_button">×</a>							<a class="wc-thumb-title" href="#">
                            <img src="assets/images/sotaigua-negre-celler-esclanya.jpg" alt="Celler Esclanyà Sotaigua Negre">Sotaigua Negre	
                        </a>
                    <span class="quantity">1 × <span class="woocommerce-Price-amount amount">185,00<span class="woocommerce-Price-currencySymbol">€</span></span></span>
                </li>

            </ul>

            <p class="woocommerce-mini-cart__total total"><strong>Subtotal:</strong> <span class="woocommerce-Price-amount amount">370,00<span class="woocommerce-Price-currencySymbol">€</span></span></p>

            <p class="woocommerce-mini-cart__buttons buttons"><a href="#" class="wc-forward">Veure el cistell</a><a href="#" class="checkout wc-forward">Finalitzar compra</a></p>

        </div>
    </div><!-- /cart-dropdown -->

</div><!-- /widget_shopping_cart -->