<section class="cd-section shop">
    <div class="container">
        <div class="text-component">
            <h2>Els Vins</h2>
        </div>
        
        <ul class="shop-grid products">
            <li class="product-card row">
                <div class="column column-66">
                    <h2 class="product_title entry-title">Blanc 2018</h2>
                    <div class="details wc-product-details">
                        <div class="entry-content">
                            <p>Etiam erat velit scelerisque in dictum non. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Tortor consequat id porta nibh venenatis cras sed felis eget.</p>
                        </div>
                    </div>

                    <a class="nota" href="#">Nota de tast</a>

                    <p class="price"><span class="woocommerce-Price-amount amount">29,00<span class="woocommerce-Price-currencySymbol">€</span></span></p>

                    <form class="cart" action="" method="post" enctype="multipart/form-data">

                        <div class="quantity">
                            <input type="number" id="" class="input-text qty text" step="1" min="1" max="9" name="quantity" value="1" title="Qty" size="4" inputmode="">
                        </div>

                        <button type="submit" name="add-to-cart" value="33" class="single_add_to_cart_button button alt">Afegir al cistell</button>

                    </form>
                </div>
                <div class="column column-33 col-image">
                    <img width="192" height="480" src="assets/images/blanc-2018-celler-esclanya.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="">
                </div>
            </li>
            <li class="product-card row">
                <div class="column column-66">
                    <h2 class="product_title entry-title">Rosat 2018</h2>
                    <div class="details wc-product-details">
                        <div class="entry-content">
                            <p>Etiam erat velit scelerisque in dictum non. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Tortor consequat id porta nibh venenatis cras sed felis eget.</p>
                        </div>
                    </div>

                    <a class="nota" href="#">Nota de tast</a>

                    <p class="price"><span class="woocommerce-Price-amount amount">29,00<span class="woocommerce-Price-currencySymbol">€</span></span></p>

                    <form class="cart" action="#" method="post" enctype="multipart/form-data">

                        <div class="quantity">
                            <input type="number" id="" class="input-text qty text" step="1" min="1" max="9" name="quantity" value="1" title="Qty" size="4" inputmode="">
                        </div>

                        <button type="submit" name="add-to-cart" value="33" class="single_add_to_cart_button button alt">Afegir al cistell</button>

                    </form>
                </div>
                <div class="column column-33 col-image">
                    <img width="200" height="500" src="assets/images/rosat-2018-celler-esclanya.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="">
                </div>
            </li>
            <li class="product-card row">
                <div class="column column-66">
                    <h2 class="product_title entry-title">Negre 2018</h2>
                    <div class="details wc-product-details">
                        <div class="entry-content">
                            <p>Etiam erat velit scelerisque in dictum non. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Tortor consequat id porta nibh venenatis cras sed felis eget.</p>
                        </div>
                    </div>

                    <a class="nota" href="#">Nota de tast</a>

                    <p class="price"><span class="woocommerce-Price-amount amount">29,00<span class="woocommerce-Price-currencySymbol">€</span></span></p>

                    <form class="cart" action="#" method="post" enctype="multipart/form-data">

                        <div class="quantity">
                            <input type="number" id="" class="input-text qty text" step="1" min="1" max="9" name="quantity" value="1" title="Qty" size="4" inputmode="">
                        </div>

                        <button type="submit" name="add-to-cart" value="33" class="single_add_to_cart_button button alt">Afegir al cistell</button>

                    </form>
                </div>
                <div class="column column-33 col-image">
                    <img width="200" height="500" src="assets/images/negre-2018-celler-esclanya.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="">
                </div>
            </li>
            <li class="product-card row">
                <div class="column column-66">
                    <h2 class="product_title entry-title">Sotaigua Negre</h2>
                    <div class="details wc-product-details">
                        <div class="entry-content">
                            <p>Etiam erat velit scelerisque in dictum non. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo. Tortor consequat id porta nibh venenatis cras sed felis eget.</p>
                        </div>
                    </div>

                    <a class="nota" href="#">Nota de tast</a>

                    <p class="price"><span class="woocommerce-Price-amount amount">59,00<span class="woocommerce-Price-currencySymbol">€</span></span></p>

                    <form class="cart" action="#" method="post" enctype="multipart/form-data">

                        <div class="quantity">
                            <input type="number" id="" class="input-text qty text" step="1" min="1" max="9" name="quantity" value="1" title="Qty" size="4" inputmode="">
                        </div>

                        <button type="submit" name="add-to-cart" value="33" class="single_add_to_cart_button button alt">Afegir al cistell</button>

                    </form>
                </div>
                <div class="column column-33 col-image">
                    <img width="200" height="500" src="assets/images/sotaigua-negre-celler-esclanya.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="">
                </div>
            </li>
        </ul>
        
        <div class="lines-shop"><div class="line"></div><div class="line"></div><div class="line"></div><div class="line"></div></div>
        
    </div>
</section>