
        <footer>
            <div class="cd-section">
                <div class="container">
                    <div class="row">
                        <div class="column column-50">
                            <h2>On som?</h2>

                            <p>Estem situats a Esclanyà dins al terme municipal de Begur (Girona). I la finca la qual hi ha el celler està composta per 16 mil m2 i conté el mas de la finca anomenat Mas Nou. Té aquest nom degut a que fou l’últim mas construït a la zona d’Esclanyà...</p>

                            <ul class="social-media">
                                <li class="location"><a href=""><svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-location"></use></svg><span>Google Maps</span></a></li>
                                <li><a href=""><svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-instagram"></use></svg><span>Instagram</span></a></li>
                                <li><a href=""><svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-facebook"></use></svg><span>Facebook</span></a></li>
                            </ul>

                        </div>

                        <div class="column column-25">
                            <h3>&nbsp;</h3>

                            <ul class="footer-menu">
                                <li><a href="#">Ajuda i FAQs</a></li>
                                <li><a href="#">Entrega i devolucions</a></li>
                                <li><a href="#">Incidències</a></li>
                                <li><a href="#">Enviaments</a></li>
                                <li><a href="#">Les nostres botigues</a></li>
                            </ul>

                        </div>

                        <div class="column column-25">
                            <h3>Subscriu-te</h3>

                            <p>I t'informarem de les nostres ofertes i novetats</p>

                            <!-- Mailchimp subscribe form -->
                            <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                <div class="footer-form newsletter-form field field-grouped is-revealing">
                                    <p class="control control-expanded">
                                        <input type="email" value="" name="EMAIL" class="input email" id="mce-EMAIL" placeholder="El teu email" required>
                                    </p>

                                    <p class="control">
                                        <input type="submit" value="Subscriu-te" name="subscribe" id="mc-embedded-subscribe" class="button button primary button-block button-shadow">
                                    </p>
                                </div>
                            </form>
                            <small>Tenim cura de les teves dades, tal com s’explica en la nostra <a href="/politica-de-privacitat/" title="Llegeix la nostra política de privacitat" target="_blank">Política de privacitat</a>.</small>
                            <!-- /Mailchimp subscribe form -->

                        </div>
                    </div>
                </div>

            </div>

            <div class="cd-section subfooter">
                <div class="container">
                    <div class="row">
                        <div class="column col-center">
                            
                            <img width="250" height="72" src="assets/images/do-emporda.jpg" class="" alt="DO Empordà logo">

                            <ul class="legal-pages">
                                <li>&copy; 2020 Celler Esclanyà</li>
                                <li><a href="#">Avís legal</a></li>
                                <li><a href="#">Política de cookies</a></li>
                                <li><a href="#">Política de privacitat</a></li>
                            </ul>

                        </div>

                    </div>
                </div>
            </div>
            
            <a id="back-to-top" title="Back to top" href="#"></a>
        </footer>
    </main>
    
    
    <script type="text/javascript" src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <!-- Preloader -->
    <script type="text/javascript">
        //<![CDATA[
		$(window).load(function() { // makes sure the whole site is loaded
			$('#status').fadeOut(); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
		})
		//]]>
    </script>
    <script type="text/javascript" src="assets/js/min/plugins.min.js"></script>
    <script type="text/javascript" src="assets/js/min/main.min.js"></script>
    
    
</body>
</html>